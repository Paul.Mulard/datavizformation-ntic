## Projet NTIC : visualisation des données sur les formations supérieures en Suisse
L'**objectif** de l'application est de **permettre aux étudiants de trouver une formation**, dans le domaine informatique, qui les intéresse, qui leur est accessible, qui leur donne un diplôme qui a de la valeur (reconnu), qui est flexible et qui leur permet d’accéder à un métier qui leur plaît.

Pour cela, nous allons utiliser une **visualisation ensembliste des formations** sous forme de cercle imbriqués (domaine informatique > sous-domaines > formations).

### TODO 
#### pour Jeudi 6 Avril
- [x] explorer D3.js
- [x] ajouter la documentation (mockups Figmas) au projet
- [x] envoyer un mail à orientation.ch pour les données
#### pour Vendredi 21 Avril
- [x] récolter les données du site web et les labelliser
- [ ] créer la base de données et y insérer les données
- [ ] faire une première version de l'application