const domains = [
    {
        "name": "Sciences informatiques",
        "formations": [
            {
                "title": "Applied Information and Data Science",
                "degree": "Master",
                "duration": 120,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Lucerne",
                    "canton": "LU"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=50522",
                "domains": [
                    "Sciences informatiques",
                    "Sciences des données"
                ]
            },
            {
                "title": "Bi-disciplinaire en sciences",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32386",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Bioinformatics and computational biology / bioinformatique et biologie computationnelle, Joint Master ",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Fribourg",
                    "canton": "FR"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=33202",
                "domains": [
                    "Bioinformatique",
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computational and Data Science",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Graubünden FHGR",
                    "city": "Coire",
                    "canton": "GR"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=71842",
                "domains": [
                    "Sciences informatiques",
                    "Sciences des données"
                ]
            },
            {
                "title": "Computational Biology and Bioinformatics, Spezialisierter Joint Master",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Bâle",
                    "canton": "BS"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32704",
                "domains": [
                    "Bioinformatique",
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität St. Gallen HSG",
                    "city": "St-Gall",
                    "canton": "SG"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=71317",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Universität Basel UNIBAS",
                    "city": "Bâle",
                    "canton": "BS"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32287",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Universität St. Gallen HSG",
                    "city": "St-Gall",
                    "canton": "SG"
                },
                "language": "Berne (BE)",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=77253",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science / Informatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Berne",
                    "canton": "BE"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32248",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science / Informatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Universität Basel UNIBAS",
                    "city": "Bâle",
                    "canton": "BS"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32145",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science / Informatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=31983",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science / Informatik",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32264",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science / Informatique",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32232",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Computer Science / Informatique, Joint Master",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Fribourg",
                    "canton": "FR"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32850",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 120,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Horw",
                    "canton": "LU"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74397",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 30,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée de Suisse occidentale HES-SO",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75076",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée bernoise (HESB)",
                    "city": "Bienne",
                    "canton": "BE"
                },
                "language": "français - allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73870",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Windisch",
                    "canton": "AG"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73134",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Zürcher Hochschule für Angewandte Wissenschaften ZHAW",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73932",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": null,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Südschweiz SUPSI",
                    "city": "Viganello",
                    "canton": "TI"
                },
                "language": "italien",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74060",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "OST - Ostschweizer Fachhochschule",
                    "city": "Rapperswil",
                    "canton": "SG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=72001",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Informatics",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Università della Svizzera italiana (USI)",
                    "city": "Lugano",
                    "canton": "TI"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=33100",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatics / Scienze informatiche",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Università della Svizzera italiana (USI)",
                    "city": "Lugano",
                    "canton": "TI"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32259",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Rotkreuz",
                    "canton": "ZG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=4116",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatik ",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Zürcher Hochschule für Angewandte Wissenschaften ZHAW",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=13592",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Windisch",
                    "canton": "AG"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=15851",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatik ",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "OST - Ostschweizer Fachhochschule",
                    "city": "Rapperswil",
                    "canton": "SG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=23550",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32847",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatik",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32848",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatik",
                "degree": "Bachelor",
                "duration": 270,
                "type": "HES",
                "location": {
                    "institution": "Fernfachhochschule Schweiz FFHS",
                    "city": "A",
                    "canton": "distance"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=44832",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatique",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32124",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatique",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée bernoise (HESB)",
                    "city": "Bienne",
                    "canton": "BE"
                },
                "language": "français - allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=15527",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Informatique",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Université de Fribourg (UNIFR)",
                    "city": "Fribourg",
                    "canton": "FR"
                },
                "language": "français - allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32158",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Life Sciences, Studienrichtung Medizininformatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Muttenz",
                    "canton": "BL"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74376",
                "domains": [
                    "Sciences informatiques",
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Mathématiques ou informatique, enseignement pour le degré secondaire II",
                "degree": "Master",
                "duration": null,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75216",
                "domains": [
                    "Mathématiques",
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Mathématiques, informatique et sciences numériques",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32377",
                "domains": [
                    "Sciences informatiques",
                    "Mathématiques"
                ]
            },
            {
                "title": "Mathématiques, informatique et sciences numériques",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32309",
                "domains": [
                    "Sciences informatiques",
                    "Mathématiques"
                ]
            },
            {
                "title": "Sciences informatiques",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Carouge",
                    "canton": "GE"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32358",
                "domains": [
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Sciences informatiques",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Carouge",
                    "canton": "GE"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32326",
                "domains": [
                    "Sciences informatiques"
                ]
            }
        ]
    },
    {
        "name": "Sciences des données",
        "formations": [
            {
                "title": "Applied Information and Data Science",
                "degree": "Master",
                "duration": 120,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Lucerne",
                    "canton": "LU"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=50522",
                "domains": [
                    "Sciences informatiques",
                    "Sciences des données"
                ]
            },
            {
                "title": "Artificial Intelligence & Machine Learning",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Rotkreuz",
                    "canton": "ZG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=66919",
                "domains": [
                    "Intelligence artificielle",
                    "Sciences des données"
                ]
            },
            {
                "title": "Computational and Data Science",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Graubünden FHGR",
                    "city": "Coire",
                    "canton": "GR"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=71842",
                "domains": [
                    "Sciences informatiques",
                    "Sciences des données"
                ]
            },
            {
                "title": "Data Science",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=47335",
                "domains": [
                    "Sciences des données"
                ]
            },
            {
                "title": "Data Science",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Zürcher Hochschule für Angewandte Wissenschaften ZHAW",
                    "city": "Winterthur",
                    "canton": "ZH"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=71654",
                "domains": [
                    "Sciences des données"
                ]
            },
            {
                "title": "Data Science",
                "degree": "Bachelor",
                "duration": 240,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Windisch",
                    "canton": "AG"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=60544",
                "domains": [
                    "Sciences des données"
                ]
            },
            {
                "title": "Data Science / Science des données",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Université de Neuchâtel (UNINE)",
                    "city": "Neuchâtel",
                    "canton": "NE"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75638",
                "domains": [
                    "Sciences des données"
                ]
            },
            {
                "title": "Data Science and Artificial Intelligence",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Südschweiz SUPSI",
                    "city": "Viganello",
                    "canton": "TI"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=69502",
                "domains": [
                    "Intelligence artificielle",
                    "Sciences des données"
                ]
            },
            {
                "title": "Data Science, Spezialisierter Master",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=69126",
                "domains": [
                    "Sciences des données"
                ]
            },
            {
                "title": "Data Science, Spezialisierter Master",
                "degree": "Master",
                "duration": null,
                "type": "HEU",
                "location": {
                    "institution": "Universität Basel UNIBAS",
                    "city": "Bâle",
                    "canton": "BS"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74915",
                "domains": [
                    "Sciences des données"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "OST - Ostschweizer Fachhochschule",
                    "city": "Rapperswil",
                    "canton": "SG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=72039",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": null,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Südschweiz SUPSI",
                    "city": "Viganello",
                    "canton": "TI"
                },
                "language": "italien",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74061",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Zürcher Hochschule für Angewandte Wissenschaften ZHAW",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73933",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Windisch",
                    "canton": "AG"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73137",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée bernoise (HESB)",
                    "city": "Bienne",
                    "canton": "BE"
                },
                "language": "français - allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73874",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 30,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée de Suisse occidentale HES-SO",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75078",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 120,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Horw",
                    "canton": "LU"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74396",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Statistics and Data Science / Statistik und Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Berne",
                    "canton": "BE"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32450",
                "domains": [
                    "Sciences des données"
                ]
            },
            {
                "title": "User Experience Design & Data Visualization",
                "degree": "Master",
                "duration": 120,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Graubünden FHGR",
                    "city": "Coire",
                    "canton": "GR"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74269",
                "domains": [
                    "Sciences des données"
                ]
            }
        ]
    },
    {
        "name": "Intelligence artificielle",
        "formations": [
            {
                "title": "Artificial Intelligence",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Università della Svizzera italiana (USI)",
                    "city": "Lugano",
                    "canton": "TI"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=48350",
                "domains": [
                    "Intelligence artificielle"
                ]
            },
            {
                "title": "Artificial Intelligence & Machine Learning",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Rotkreuz",
                    "canton": "ZG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=66919",
                "domains": [
                    "Intelligence artificielle",
                    "Sciences des données"
                ]
            },
            {
                "title": "Artificial Intelligence / Intelligence artificielle",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "UniDistance Suisse",
                    "city": "A",
                    "canton": "distance"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=60933",
                "domains": [
                    "Intelligence artificielle"
                ]
            },
            {
                "title": "Artificial Intelligence in Medicine / Künstliche Intelligenz in der Medizin, Spezialisierter Master",
                "degree": "Master",
                "duration": 60,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Berne",
                    "canton": "BE"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=72639",
                "domains": [
                    "Intelligence artificielle",
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Data Science and Artificial Intelligence",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Südschweiz SUPSI",
                    "city": "Viganello",
                    "canton": "TI"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=69502",
                "domains": [
                    "Intelligence artificielle",
                    "Sciences des données"
                ]
            }
        ]
    },
    {
        "name": "Informatique pour la médecine",
        "formations": [
            {
                "title": "Artificial Intelligence in Medicine / Künstliche Intelligenz in der Medizin, Spezialisierter Master",
                "degree": "Master",
                "duration": 60,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Berne",
                    "canton": "BE"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=72639",
                "domains": [
                    "Intelligence artificielle",
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Digital Neuroscience / Neurosciences digitales, Master spécialisé",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Fribourg (UNIFR)",
                    "city": "Fribourg",
                    "canton": "FR"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74645",
                "domains": [
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Life Sciences, Studienrichtung Medizininformatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Muttenz",
                    "canton": "BL"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74376",
                "domains": [
                    "Sciences informatiques",
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Medical Informatics ",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Muttenz",
                    "canton": "BL"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=61849",
                "domains": [
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Medizininformatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée bernoise (HESB)",
                    "city": "Bienne",
                    "canton": "BE"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=14290",
                "domains": [
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Medizininformatik",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Zürcher Hochschule für Angewandte Wissenschaften ZHAW",
                    "city": "Winterthur",
                    "canton": "ZH"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=77382",
                "domains": [
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Neural Systems and Computation, Joint Master, Spezialisierter Master",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32862",
                "domains": [
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Neuroinformatik, Nur-Nebenfach",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32446",
                "domains": [
                    "Informatique pour la médecine"
                ]
            },
            {
                "title": "Traitement informatique multilingue",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=43845",
                "domains": [
                    "Informatique pour la médecine"
                ]
            }
        ]
    },
    {
        "name": "Bioinformatique",
        "formations": [
            {
                "title": "Bioinformatics / Bioinformatik, Nur-Nebenfach",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=33068",
                "domains": [
                    "Bioinformatique"
                ]
            },
            {
                "title": "Bioinformatics and computational biology / bioinformatique et biologie computationnelle, Joint Master ",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Fribourg",
                    "canton": "FR"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=33202",
                "domains": [
                    "Bioinformatique",
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Bioinformatik, Nur-Nebenfach",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32445",
                "domains": [
                    "Bioinformatique"
                ]
            },
            {
                "title": "Computational Biology and Bioinformatics, Spezialisierter Joint Master",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Bâle",
                    "canton": "BS"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32704",
                "domains": [
                    "Bioinformatique",
                    "Sciences informatiques"
                ]
            }
        ]
    },
    {
        "name": "Systèmes d'information",
        "formations": [
            {
                "title": "Communication Systems / Systèmes de communication",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32233",
                "domains": [
                    "Systèmes d'information"
                ]
            },
            {
                "title": "Computational Social Sciences",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Luzern UNILU",
                    "city": "Lucerne",
                    "canton": "LU"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=63433",
                "domains": [
                    "Systèmes d'information"
                ]
            },
            {
                "title": "Information & Cyber Security",
                "degree": "Bachelor",
                "duration": null,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Rotkreuz",
                    "canton": "ZG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=59286",
                "domains": [
                    "Systèmes d'information",
                    "Sécurité numérique"
                ]
            },
            {
                "title": "Information Systems and Digital Innovation / Systèmes d'information et innovation numérique",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Université de Lausanne (UNIL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32336",
                "domains": [
                    "Systèmes d'information"
                ]
            },
            {
                "title": "Informatique et systèmes de communication",
                "degree": "Bachelor",
                "duration": 240,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée de Suisse occidentale HES-SO",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français - allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=69148",
                "domains": [
                    "Systèmes d'information"
                ]
            },
            {
                "title": "International IT Management",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Rotkreuz",
                    "canton": "ZG"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=58956",
                "domains": [
                    "Systèmes d'information"
                ]
            },
            {
                "title": "Law in Legal Issues, Crime and Security of Information Technologies / Droit, criminalité et sécurité des technologies de l'information",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Lausanne (UNIL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32574",
                "domains": [
                    "Systèmes d'information",
                    "Sécurité numérique"
                ]
            },
            {
                "title": "Robotics, Systems and Control (Robotik), spezialisierter Master",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32939",
                "domains": [
                    "Robotique",
                    "Systèmes d'information"
                ]
            },
            {
                "title": "Systèmes de communication",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32131",
                "domains": [
                    "Systèmes d'information"
                ]
            },
            {
                "title": "Systèmes d'information et science des services",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Centre Universitaire d'informatique (CUI)",
                    "city": "Carouge",
                    "canton": "GE"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32475",
                "domains": [
                    "Systèmes d'information"
                ]
            },
            {
                "title": "Systèmes et services numériques",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Centre Universitaire d'informatique (CUI)",
                    "city": "Carouge",
                    "canton": "GE"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75055",
                "domains": [
                    "Systèmes d'information",
                    "Ingéniérie informatique"
                ]
            }
        ]
    },
    {
        "name": "Informatique pour la linguistique",
        "formations": [
            {
                "title": "Computational Linguistics and Language Technology",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32649",
                "domains": [
                    "Informatique pour la linguistique"
                ]
            },
            {
                "title": "Computerlinguistik und Sprachtechnologie",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32648",
                "domains": [
                    "Informatique pour la linguistique"
                ]
            },
            {
                "title": "Digital Linguistics / Digitale Linguistik, Nur-Nebenfach",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Zürich UZH",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=61632",
                "domains": [
                    "Informatique pour la linguistique"
                ]
            }
        ]
    },
    {
        "name": "Ingéniérie informatique",
        "formations": [
            {
                "title": "Digital Engineering",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Horw",
                    "canton": "LU"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=68783",
                "domains": [
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 120,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Horw",
                    "canton": "LU"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74397",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 30,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée de Suisse occidentale HES-SO",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75076",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée bernoise (HESB)",
                    "city": "Bienne",
                    "canton": "BE"
                },
                "language": "français - allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73870",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Windisch",
                    "canton": "AG"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73134",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Zürcher Hochschule für Angewandte Wissenschaften ZHAW",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73932",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": null,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Südschweiz SUPSI",
                    "city": "Viganello",
                    "canton": "TI"
                },
                "language": "italien",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74060",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Computer Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "OST - Ostschweizer Fachhochschule",
                    "city": "Rapperswil",
                    "canton": "SG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=72001",
                "domains": [
                    "Sciences informatiques",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "OST - Ostschweizer Fachhochschule",
                    "city": "Rapperswil",
                    "canton": "SG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=72039",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": null,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Südschweiz SUPSI",
                    "city": "Viganello",
                    "canton": "TI"
                },
                "language": "italien",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74061",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Zürcher Hochschule für Angewandte Wissenschaften ZHAW",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73933",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Nordwestschweiz FHNW",
                    "city": "Windisch",
                    "canton": "AG"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73137",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 90,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée bernoise (HESB)",
                    "city": "Bienne",
                    "canton": "BE"
                },
                "language": "français - allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73874",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 30,
                "type": "HES",
                "location": {
                    "institution": "Haute école spécialisée de Suisse occidentale HES-SO",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75078",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Engineering, Profil Data Science",
                "degree": "Master",
                "duration": 120,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Horw",
                    "canton": "LU"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=74396",
                "domains": [
                    "Sciences des données",
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Ingegneria informatica",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HES",
                "location": {
                    "institution": "Fachhochschule Südschweiz SUPSI",
                    "city": "Viganello",
                    "canton": "TI"
                },
                "language": "italien",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=31370",
                "domains": [
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Software and Data Engineering",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Università della Svizzera italiana (USI)",
                    "city": "Lugano",
                    "canton": "TI"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=48352",
                "domains": [
                    "Ingéniérie informatique"
                ]
            },
            {
                "title": "Systèmes et services numériques",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Centre Universitaire d'informatique (CUI)",
                    "city": "Carouge",
                    "canton": "GE"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75055",
                "domains": [
                    "Systèmes d'information",
                    "Ingéniérie informatique"
                ]
            }
        ]
    },
    {
        "name": "Informatique pour les sciences humaines",
        "formations": [
            {
                "title": "Digital Humanities",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Basel UNIBAS",
                    "city": "Bâle",
                    "canton": "BS"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=61112",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            },
            {
                "title": "Digital Humanities / Humanités digitales, Master spécialisé",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=39963",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            },
            {
                "title": "Digital Humanities / Humanités numériques",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Lausanne (UNIL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=47822",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            },
            {
                "title": "Digital Humanities, Nur-Nebenfach",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Universität Bern UNIBE",
                    "city": "Berne",
                    "canton": "BE"
                },
                "language": "allemand - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=73506",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            },
            {
                "title": "Informatique pour les sciences humaines",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32606",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            },
            {
                "title": "Informatique pour les sciences humaines",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32637",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            },
            {
                "title": "Informatique pour les sciences humaines (ISH)",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Lausanne (UNIL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32768",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            },
            {
                "title": "Informatique pour les sciences humaines (ISH)",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Université de Lausanne (UNIL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32112",
                "domains": [
                    "Informatique pour les sciences humaines"
                ]
            }
        ]
    },
    {
        "name": "Sécurité numérique",
        "formations": [
            {
                "title": "Information & Cyber Security",
                "degree": "Bachelor",
                "duration": null,
                "type": "HES",
                "location": {
                    "institution": "Hochschule Luzern HSLU",
                    "city": "Rotkreuz",
                    "canton": "ZG"
                },
                "language": "allemand",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=59286",
                "domains": [
                    "Systèmes d'information",
                    "Sécurité numérique"
                ]
            },
            {
                "title": "Informatique, majeure en Cyber Security, Joint Master",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=63478",
                "domains": [
                    "Sécurité numérique"
                ]
            },
            {
                "title": "Law in Legal Issues, Crime and Security of Information Technologies / Droit, criminalité et sécurité des technologies de l'information",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Lausanne (UNIL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32574",
                "domains": [
                    "Systèmes d'information",
                    "Sécurité numérique"
                ]
            }
        ]
    },
    {
        "name": "Mathématiques",
        "formations": [
            {
                "title": "Mathématiques ou informatique, enseignement pour le degré secondaire II",
                "degree": "Master",
                "duration": null,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=75216",
                "domains": [
                    "Mathématiques",
                    "Sciences informatiques"
                ]
            },
            {
                "title": "Mathématiques, informatique et sciences numériques",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français - anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32377",
                "domains": [
                    "Sciences informatiques",
                    "Mathématiques"
                ]
            },
            {
                "title": "Mathématiques, informatique et sciences numériques",
                "degree": "Bachelor",
                "duration": 180,
                "type": "HEU",
                "location": {
                    "institution": "Université de Genève (UNIGE)",
                    "city": "Genève",
                    "canton": "GE"
                },
                "language": "français",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32309",
                "domains": [
                    "Sciences informatiques",
                    "Mathématiques"
                ]
            }
        ]
    },
    {
        "name": "Robotique",
        "formations": [
            {
                "title": "Robotics / Robotique",
                "degree": "Master",
                "duration": 120,
                "type": "HEU",
                "location": {
                    "institution": "Ecole polytechnique fédérale de Lausanne (EPFL)",
                    "city": "Lausanne",
                    "canton": "VD"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=50663",
                "domains": [
                    "Robotique"
                ]
            },
            {
                "title": "Robotics, Systems and Control (Robotik), spezialisierter Master",
                "degree": "Master",
                "duration": 90,
                "type": "HEU",
                "location": {
                    "institution": "Eidgenössische Technische Hochschule Zürich ETHZ",
                    "city": "Zurich",
                    "canton": "ZH"
                },
                "language": "anglais",
                "link": "orientation.ch/dyn/show/4009?lang=fr&idx=120&id=32939",
                "domains": [
                    "Robotique",
                    "Systèmes d'information"
                ]
            }
        ]
    }
]