const express = require("express");
const cors = require('cors');
const graphdb = require("graphdb42");

const app = express();
const port = 3000;

const formatCollection = (collection) => {
  let counter = 1;
  collection.forEach(element => {
    element.identifier = element.id;
    element.id = counter;
    counter += 1;
  });
  return collection;
}

const createSimplifierMap = (collection) => {
  const getElement = {};
  collection.forEach(element => {
    getElement[element.id] = element;
  });
  return getElement;
}

const createJsonifier = (collection, getElement) => {
  return (id = null) => {
    if (id === null){
      return JSON.stringify(
        collection.map((element) => {
          const deepcopy = JSON.parse(JSON.stringify(element));
          delete deepcopy.identifier;
          return deepcopy;
        }));
    }
    else{
      const deepcopy = JSON.parse(JSON.stringify(getElement[id]));
      delete deepcopy.identifier;
      return JSON.stringify(deepcopy);
    }
  }
}

const domains = formatCollection(graphdb.getDomains());
const getDomain = createSimplifierMap(domains);
domains.getJson = createJsonifier(domains, getDomain);

const cantons = formatCollection(graphdb.getCantons());
const getCanton = createSimplifierMap(cantons);
cantons.getJson = createJsonifier(cantons, getCanton);


app.use(cors());

app.get("/domains", (_, res) => {
    res.send(domains.getJson());
});

app.get("/domains/:idDomain", (req, res) => {
    res.send(domains.getJson(req.params.idDomain));
});

app.get("/domains/:idDomain/formations", (req, res) => {
    const formations = graphdb.getFormationsByDomain(getDomain[req.params.idDomain].identifier);
    formations.forEach(formation => delete formation.id);
    res.send(JSON.stringify(formations));
});

app.get("/cantons", (_, res) => {
  res.send(cantons.getJson());
});

app.get("/cantons/:idCanton", (req, res) => {
  res.send(cantons.getJson(req.params.idCanton));
});

app.get("/cantons/:idCanton/formations", (req, res) => {
  const formations = graphdb.getFormationsByCantons(getCanton[req.params.idCanton].identifier);
  formations.forEach(formation => delete formation.id);
  res.send(JSON.stringify(formations));
});

app.listen(port, () => {
  console.log(`All done ! DatavizFormationCH backend is listening...`);
});