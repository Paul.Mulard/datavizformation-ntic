const formations = require('./data/formations.json');
const rdf = require('rdflib');

// We create a rdf-graph of formations based on the data in the json file
// Jpeg representation of the graph is in the folder


// 1. SETUP THE GRAPH
const store = rdf.graph();

const FCH = new rdf.Namespace('http://unige.ch/ntic/formationch#');
const RDF = new rdf.Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
const RDFS = new rdf.Namespace('http://www.w3.org/2000/01/rdf-schema#');

const predicates = {
    label: RDFS('label'),
    givenInLanguage: FCH('givenInLanguage'),
    ofDegree: FCH('ofDegree'),
    givenBy: FCH('givenBy'),
    link: FCH('link'),
    inTheCantonOf: FCH('inTheCantonOf'),
    inTheFieldOf: FCH('inTheFieldOf'),
    durationInECTS: FCH('durationInECTS'),
}

const domains = {
    'Sciences des données': FCH('Sciencesdesdonnees'),
    'Sciences informatiques': FCH('Sciencesinformatiques'),
    "Systèmes d'information" : FCH("Systemesd'information"),
    "Intelligence artificielle": FCH("Intelligenceartificielle"),
    "Informatique pour la médecine": FCH("Informatiquepourlamedecine"),
    "Bioinformatique": FCH("Bioinformatique"),
    "Informatique pour la linguistique": FCH("Informatiquepourlalinguistique"),
    "Ingéniérie informatique": FCH("Ingenierieinformatique"),
    "Informatique pour les sciences humaines": FCH("Informatiquepourlesscienceshumaines"),
    "Robotique": FCH("Robotique"),
    "Sécurité numérique": FCH("Securitenumerique"),
    "Mathématiques": FCH("Mathematiques"),
};
Object.keys(domains).forEach(dom => {
    store.add(domains[dom],predicates.label,dom);
});
    
const FCHformation = (title, institution) => FCH(`${title.replace(/\s/g,'')}_${institution.replace(/\s/g,'')}`);
const FCHinstitution = (institution) => FCH(institution.replace(/\s/g,''));
formations.forEach(data => {
    const formation = FCHformation(data.title, data.location.institution);
    const institution = FCHinstitution(data.location.institution);

    store.add(formation,predicates.label,data.title);
    store.add(formation,predicates.givenInLanguage,data.language);
    store.add(formation,predicates.ofDegree,data.degree);
    store.add(formation,predicates.link,data.link);
    store.add(formation,predicates.givenBy,institution);
    store.add(formation,predicates.inTheCantonOf,data.location.canton);
    if (data.duration)
        store.add(formation,predicates.durationInECTS,data.duration);

    store.add(institution,predicates.label,data.location.institution);

    data.domains.forEach(dom =>{
        store.add(formation,predicates.inTheFieldOf,domains[dom]);
    });
});

// 2. DEFINE QUERIES ON THE GRAPH
const getGroups = (predicate, formatFunction) => {
    return () => {
        const result = Object();
        const statements = store.match(null,predicate,null);
        statements.forEach(statement => {
            const attribute = statement.object;
            if (attribute in result){
                result[attribute].count += 1;
            }
            else{
                const newd = formatFunction(store, attribute);
                result[attribute] = newd;
                result[attribute].count = 1;
            }
        });
        return Object.keys(result).map(key => result[key]);
    }
}

const formatDomain = (db, dom) => {
    const result = Object();
    result.id = dom;
    const statements = db.match(dom, null, null);
    statements.forEach(statement => {
        switch (statement.predicate) {
            case predicates.label:
                result.name = statement.object.value;
                break;
        }
    });
    return result;
}

const getDomains = getGroups(predicates.inTheFieldOf, formatDomain);

const formatCanton = (_, canton) => {
    const result = Object();
    result.id = canton;
    result.name = canton.value;
    return result;
}

const getCantons = getGroups(predicates.inTheCantonOf, formatCanton);

const formatFormation = (db, form) => {
    const result = Object();
    result.id = form.value;
    const statements = db.match(form, null, null);
    statements.forEach(statement => {
        switch (statement.predicate) {
            case predicates.link:
                result.link = statement.object.value;
                break;
            case predicates.ofDegree:
                result.degree = statement.object.value;
                break;
            case predicates.givenInLanguage:
                result.lang = statement.object.value;
                break;
            case predicates.label:
                result.title = statement.object.value;
                break;
            case predicates.durationInECTS:
                result.creditsECTS = statement.object.value;
                break;
            case predicates.inTheCantonOf:
                result.canton = statement.object.value;
                break;
        }
    });
    return result;
}

const getFormations = (predicate) => {
    return (value) => {
        const result = [];
        const statements = store.match(null, predicate, value);
        statements.forEach(statement => {
            const formation = statement.subject;
            result.push(formation);
        });
        return result.map(r => formatFormation(store, r));
    }
}

const getFormationsByDomain = getFormations(predicates.inTheFieldOf);
const getFormationsByCantons = getFormations(predicates.inTheCantonOf);

// 3. EXPORT API
module.exports = {
   getDomains: getDomains,
   getFormationsByDomain: getFormationsByDomain,
   getCantons: getCantons,
   getFormationsByCantons: getFormationsByCantons,
}