import json

with open("./formations_without_duplicata.json", "r") as f:
    data = json.load(f)

domains = {}
for formation in data:
    for domain in formation["domains"]:
        if domain not in domains:
            domains[domain] = []
        domains[domain].append(formation)

data = []
for domain in domains:
    data.append({
        "name": domain,
        "formations": domains[domain]
    })

with open("./domains.json", "w") as f:
    json.dump(data, f, indent=4, ensure_ascii=False)