import json

def main():
    with open('./formations.json', 'r') as f:
        data = json.load(f)

    formations = []
    for formation in data:
        if not any(are_equals(formation, f) for f in formations):
            formations.append(formation)
    
    print(len(data))
    print(len(formations))

    with open('./formations_without_duplicata.json', 'w') as f:
        json.dump(formations, f, indent=4, ensure_ascii=False)

def are_equals(formation1, formation2):
    if (
        formation1["title"] != formation2["title"] or
        formation1["degree"] != formation2["degree"] or
        formation1["location"]["institution"] != formation2["location"]["institution"]
    ):
        return False
    return True

main()