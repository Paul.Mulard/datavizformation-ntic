import json

with open("./formations_without_duplicata.json", "r") as f:
    data = json.load(f)

cantons = {}
for formation in data:
    canton = formation["location"]["canton"]
    if canton not in cantons:
        cantons[canton] = []
    cantons[canton].append(formation)

data = []
for canton in cantons:
    data.append({
        "name": canton,
        "formations": cantons[canton]
    })

with open("./cantons.json", "w") as f:
    json.dump(data, f, indent=4, ensure_ascii=False)