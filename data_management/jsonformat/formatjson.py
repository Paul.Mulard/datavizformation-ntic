import json

def getdomains():
    with open('jsonformat/formations.json', 'r') as f:
        formations = json.load(f)

    result = []
    for formation in formations['data']:
        result.append({
            'title': formation['title'],
            'domains': formation['domains']
        })

    with open('formations_domains.json', 'w') as f:
        json.dump(result, f, ensure_ascii=False, indent=2)

def mergeversion():
    with open('jsonformat/formations_domains.json', 'r') as f:
        formations_domains = json.load(f)
    with open('jsonformat/formations_v2.json', 'r') as f:
        formations_v2 = json.load(f)
    
    result = []
    for formation in formations_v2['data']:
        for formation_domain in formations_domains:
            if formation_domain['title'] == formation['title']:
                newdata = formation
                newdata['domains'] = formation_domain['domains']
                result.append(newdata)
    
    with open('formations_v3.json', 'w') as f:
        json.dump(result, f, ensure_ascii=False, indent=2)

def formatlocation():
    with open('formations_v3.json', 'r') as f:
        formations_v3 = json.load(f)
    
    for formation in formations_v3:
        location = formation["location"]["city"].split()
        formation["location"]["city"] = location[0]
        indexCanton = 1
        if location[indexCanton].isnumeric():
            indexCanton = 2
        formation["location"]["canton"] = location[indexCanton].replace("(","").replace(")","")
    
    with open('formations_v4.json', 'w') as f:
        json.dump(formations_v3, f, ensure_ascii=False, indent=2)

formatlocation()