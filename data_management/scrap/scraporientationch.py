import re, json, requests
from bs4 import BeautifulSoup

with open("./scrap/formations.html","r") as filer:
    formations = BeautifulSoup(filer.read(), 'html.parser')

# le scrapping de la page web
def getformations(formations, c='result'):
    formations_=[]
    formations = formations.find_all('div', class_=c)
    for _, formation in enumerate(formations) : 
        title = formation.find('a',class_='ajaxUpadteHrefIdx')
        link = 'orientation.ch'+title['href']
        degree = formation.find('span', class_='disc')
        (degree, he) = degree.text.split(' ')
        institution = formation.find('span', class_='ref')
        location = formation.find('dd')
        lang = location.find_next('dd')
        location = location.text.replace('\n','')
        formations_.append({
            "title": title.text,
            "degree":  degree,
            "duration": getmoreinformations(link)['duration'],
            "type": he,
            "location": {
                "institution": institution.text,
                "city": location,
            },
            "language": lang.text.replace('\n',''),
            "link": link
        })
    return formations_

def getmoreinformations(url):
    formation = BeautifulSoup(requests.get(f'https://{url}').text, 'html.parser')
    infos = formation.find('div',id='anchor6').find_next('div')
    duration = infos.find('h4').find_next('h4').find_next('p')
    return {
        "duration": getcredits(duration.text)
    }

# la labélisation utilisant les noms de formation     
def getlabel(name):
    labels = {
        'Intelligence artificielle': r"(?i)\b(?:.*artificiel(?:s|les)?|.*artificial|.*künstliche?s?|.*intelligence(?:s)?|.*intelligent|intelligenz|.*synthétique(?:s)?|.*synthetic|.*synthetische|.*synthétisé(?:es)?|.*synthesized|.*synthetisiert)\b",
        'Sciences informatiques': r"(?i)\b(?:.*sciences?|.*science|.*wissenschaft(?:en)?|.*connaissances?(?:s)?|.*connaissance(?:s)?|.*knowledges?(?:s)?|.*knowledge|.*know-hows?(?:s)?|.*know-how)\b",
        'Systèmes d\'information':  r"(?i)\b(?:.*informations?|.*information|.*informationen|.*renseignements?|.*informationssysteme|.*informationssystemen|.*information systems|.*syst[eè]me(?:s)? d'information)\b",
        'Bioinformatique': r"(?i)\b(?:.*bioinformatique(?:s)?|.*bioinformatics?|.*bioinformatiks?)\b",
        'Sciences des données': r"(?i)\b(?:.*donn[eé]es?|.*data|.*datas?(?:s)?|.*daten)\b",
        'Sécurité numérique': r"(?i)\b(?:.*s[eé]curité|.*security|.*sicherheit)\b",
        'Informatique pour les sciences humaines': r"(?i)\b(?:.*humain(?:e)?s?|.*human(?:s)?|.*menschlich[en]?|.*sciences? humaines?(?:s)?|.*humanit[eé]s?|.*humanities|.*geisteswissenschaften)\b",
        'Mathématiques': r"(?i)\b(?:.*math[eé]matique(?:s)?|.*mathematics|.*mathematik)\b",
        'Informatique pour la médecine': r"(?i)\b(?:.*m[eé]decine(?:s)?|.*medicine(?:s)?|.*medizin(?:s)?|.*neuroscience|.*neurosciences|.*neuroinformatique(?:s)?|.*neuroinformatics?|.*neuroinformatiks?|.*informatique(?:s)? pour la médecine)\b",
        'Informatique pour la linguistique': r"(?i)\b(?:.*linguistique?s?|.*linguistics?|.*linguistik|.*sprachwissenschaft(?:en)?|.*informatique(s)? pour la linguistique)\b",
        'Ingéniérie informatique': r"(?i)\b(?:.*(?:ing[eé]nieur[e]?s?|engineer(?:s)?|ingenieur(?:e)?s?|technicien(?:s)?|technicienne(?:s)?|ing[eé]ni[eè]rie|engineering|ingenieurwesen).*\b)",
    }


    return [n for n, r in labels.items() if re.search(r, name)]

def getcredits(durationraw):
    motifs_semestre = ['semestre', 'semestres', 'semester', 'semesters', 'semestre', 'semestern', 'semestres','semestri']
    motifs_annee = ['année', 'années', 'year', 'years', 'jahr', 'jahre', 'anno', 'anni']
    nombres = {
        'eins': 1,
        'zwei': 2,
        'drei': 3,
        'vier': 4,
        'fünf': 5,
        'sechs': 6,
        'sieben': 7,
        'acht': 8,
        'neun': 9,
        'zehn': 10,
        'uno': 1,
        'due': 2,
        'tre': 3,
        'quattro': 4,
        'cinque': 5,
        'sei': 6,
        'sette': 7,
        'otto': 8,
        'nove': 9,
        'dieci': 10,
         'un': 1,
        'deux': 2,
        'trois': 3,
        'quatre': 4,
        'cinq': 5,
        'six': 6,
        'sept': 7,
        'huit': 8,
        'neuf': 9,
        'dix': 10
    }

    regex_semestre = r''+('|'.join(motifs_semestre))
    regex_annee = r''+('|'.join(motifs_annee))
    
    duration = {
        'y':[],
        's':[]
    }
    wordlist = durationraw.split(' ')
    for c, w in enumerate(wordlist):
        if re.search(regex_annee, w, re.IGNORECASE):
            if wordlist[c-1] in nombres:
                duration['y'].append(nombres[wordlist[c-1]])
            elif wordlist[c-1].isnumeric():
                duration['y'].append(int(wordlist[c-1]))
        elif re.search(regex_semestre, w, re.IGNORECASE):
            if wordlist[c-1] in nombres:
                duration['s'].append(nombres[wordlist[c-1]])
            elif wordlist[c-1].isnumeric():
                duration['s'].append(int(wordlist[c-1]))

    if len(duration['s']) >= 1 :
        return min(duration['s'])*30
    elif len(duration['y']) >= 1 :
        return min(duration['y'])*30*2
    else : 
        return None
    

f = {'data': getformations(formations)}
with open('formations.json','w') as filew : 
    filew.write(json.dumps(f,indent=4,ensure_ascii=False))
